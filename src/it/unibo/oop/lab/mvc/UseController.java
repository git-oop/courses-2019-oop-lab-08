package it.unibo.oop.lab.mvc;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class UseController implements Controller{

    private final List<String> stringHistory = new LinkedList<String>();
    private String nextString;
    
    public void setNextString(String string) {
        this.nextString = Objects.requireNonNull(string, "This method does not accept null values.");
    }

    
    public String getNextString() {
        return this.nextString;
    }

    
    public List<String> showStringHystory() {
        return this.stringHistory;
    }

    public void printCurrentString() {
        if(this.nextString==null) {
            throw new IllegalStateException("String is null.");
        }
        this.stringHistory.add(this.nextString);
        System.out.println(this.nextString);
    }

}
