package it.unibo.oop.lab.mvcio;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import it.unibo.oop.lab.iogui.BadIOGUI;

/**
 * 
 */
public class Controller {
    private File currentFile;
    private static final String path = System.getProperty("user.home")
            + System.getProperty("file.separator")
            + BadIOGUI.class.getSimpleName() + ".txt";
    
    public Controller() {
        
        this.currentFile = new File(path);
    }
    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     * 
     * Implement this class with:
     * 
     * 1) A method for setting a File as current file
     * 
     * 2) A method for getting the current File
     * 
     * 3) A method for getting the path (in form of String) of the current File
     * 
     * 4) A method that gets a String as input and saves its content on the current
     * file. This method may throw an IOException.
     * 
     * 5) By default, the current file is "output.txt" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that runs correctly on every platform.
     */
    
    public void setFile(String path) throws IOException {
       File tmp = new File(path);
       this.currentFile = tmp;
       System.out.println(this.currentFile.getPath());
       return;
    }
    
    public File getFile() {
        return this.currentFile;
    }
    
    public String getPath() {
        return this.currentFile.getPath();
    }
    
    public void writeString(String text) throws IOException {
        try {
            PrintWriter writer = new PrintWriter(this.currentFile.getAbsolutePath(), "UTF-8");
            writer.println(text);
            writer.close();
        }catch (IOException e) {
            e.getStackTrace();
        }
    }
}
